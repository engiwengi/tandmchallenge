using System;


namespace TandmChallenge
{
    public class LineSegment3d
    {
        public Point3d start;
        public Point3d end;


        public Point3d ClosestPointOnLineToPoint(Point3d point)
        {
            var percent = (point - this.start).Dot(this.end - this.start) / (this.end - this.start).SquaredMagnitude();
            return this.PointAtPercentage(percent);
        }

        private Point3d PointAtPercentage(double percentage)
        {
            if (percentage <= 0)
            {
                return this.start;
            }
            else if (percentage >= 1)
            {
                return this.end;
            }
            else
            {
                return (this.start + (this.end - this.start) * percentage);
            }
        }
    }
}
