using System;

using System.Collections.Generic;

namespace TandmChallenge
{
    public class Polygon2d
    {
        public List<Point2d> vertices;

        public Tuple<Point2d, Point2d> boundingBox;


        public Polygon2d(List<Point2d> vertices)
        {
            this.vertices = vertices;
            this.boundingBox = Tuple.Create(new Point2d(double.MaxValue, double.MaxValue), new Point2d(double.MinValue, double.MinValue));
            foreach (var vertex in this.vertices)
            {
                this.boundingBox.Item1.X = Math.Min(this.boundingBox.Item1.X, vertex.X);
                this.boundingBox.Item1.Y = Math.Min(this.boundingBox.Item1.Y, vertex.Y);
                this.boundingBox.Item2.X = Math.Max(this.boundingBox.Item2.X, vertex.X);
                this.boundingBox.Item2.Y = Math.Max(this.boundingBox.Item2.Y, vertex.Y);
            }
        }

        public bool ContainsPoint(Point2d point)
        {
            var ray = new LineSegment2d() { start = point, end = new Point2d(this.boundingBox.Item1.X, point.Y) };
            int intersectionCount = 0;
            if (this.vertices.Count > 1)
            {
                var previous = this.vertices[this.vertices.Count - 1];
                foreach (var next in this.vertices)
                {
                    var line = new LineSegment2d() { start = previous, end = next };
                    if (line.Intersects(ray))
                    {
                        intersectionCount++;
                    }
                    previous = next;
                }
            }
            return intersectionCount % 2 == 0 ? false : true;
        }

        public Tuple<bool, double> ContainsPointMinSquaredDistance(Point2d point)
        {
            var ray = new LineSegment2d() { start = point, end = new Point2d(this.boundingBox.Item1.X, point.Y) };
            int intersectionCount = 0;
            double squaredDistance = double.MaxValue;
            if (this.vertices.Count > 1)
            {
                var previous = this.vertices[this.vertices.Count - 1];
                foreach (var next in this.vertices)
                {
                    var line = new LineSegment2d() { start = previous, end = next };
                    if (line.Intersects(ray))
                    {
                        intersectionCount++;
                    }

                    double squaredDistanceFromWall = (point - line.ClosestPointOnLineToPoint(point)).SquaredMagnitude();

                    if (squaredDistance > squaredDistanceFromWall)
                    {
                        squaredDistance = squaredDistanceFromWall;
                    }

                    previous = next;
                }
            }
            return Tuple.Create(intersectionCount % 2 == 0 ? false : true, squaredDistance);
        }
    }
}
