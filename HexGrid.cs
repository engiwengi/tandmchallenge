using System;
using System.Collections.Generic;

namespace TandmChallenge
{
    public class HexGrid
    {
        public const double SideLength = 2500.00;

        public Point2d translation;
        public double angle;


        public void OptimizeForLeastVerticesInscribedByPolygon(Polygon2d polygon)
        {
            int bestNumVertices = int.MaxValue;
            double bestAngle = 0.0;
            var bestTranslation = new Point2d(0, 0);
            double bestDistanceFromEdge = 0.0;

            for (double angle = 0.0; angle < Math.PI / 3; angle += 0.02)
            {
                for (double x = 0.0; x < 1.0; x += 0.02)
                {
                    for (double y = 0.0; y < 1.0; y += 0.02)
                    {
                        this.angle = angle;
                        this.translation = new Point2d(x, y);
                        var iter = this.VerticesInBoundingBox(polygon.boundingBox);
                        int inscribedVertices = 0;
                        double minDistanceFromEdge = double.MaxValue;
                        foreach (var sprinkler in iter)
                        {
                            var containsSquaredDistance = polygon.ContainsPointMinSquaredDistance(sprinkler);
                            if (containsSquaredDistance.Item1)
                            {
                                inscribedVertices++;
                                minDistanceFromEdge = Math.Min(minDistanceFromEdge, containsSquaredDistance.Item2);
                            }
                        }
                        if (bestNumVertices > inscribedVertices || bestNumVertices == inscribedVertices && minDistanceFromEdge > bestDistanceFromEdge)
                        {
                            bestDistanceFromEdge = minDistanceFromEdge;
                            bestNumVertices = inscribedVertices;
                            bestAngle = angle;
                            bestTranslation = this.translation;
                        }
                    }
                }
            }

            this.angle = bestAngle;
            this.translation = bestTranslation;
        }

        public IEnumerable<Point2d> VerticesInBoundingBox(Tuple<Point2d, Point2d> boundingBox)
        {
            var midPoint = (boundingBox.Item1 + boundingBox.Item2) / 2.0;
            double root3Over2 = Math.Sqrt(3) / 2;
            double cosAngle = Math.Cos(this.angle);
            double sinAngle = Math.Sin(this.angle);

            // TODO improve method of finding hexagon vertex coord points in bounding box
            double width = boundingBox.Item2.X - boundingBox.Item1.X;
            double height = boundingBox.Item2.Y - boundingBox.Item1.Y;
            int n = (int)Math.Ceiling(Math.Max(width, height) / (SideLength));
            for (int x = -n / 2; x < n / 2; x++)
            {
                for (int y = -n / 2; y < n / 2; y++)
                {
                    var centre = new Point2d((x - y) * 3.0 / 2.0, root3Over2 * (x + y));
                    var point1 = (new Point2d(1.0 + centre.X + translation.X, centre.Y + translation.Y) * SideLength).RotatePrecalculated(sinAngle, cosAngle) + midPoint;
                    if (point1.X >= boundingBox.Item1.X && point1.Y >= boundingBox.Item1.Y && point1.X <= boundingBox.Item2.X && point1.Y <= boundingBox.Item2.Y)
                    {
                        yield return point1;
                    }
                    var point2 = (new Point2d(0.5 + centre.X + translation.X, root3Over2 + centre.Y + translation.Y) * SideLength).RotatePrecalculated(sinAngle, cosAngle) + midPoint;
                    if (point2.X >= boundingBox.Item1.X && point2.Y >= boundingBox.Item1.Y && point2.X <= boundingBox.Item2.X && point2.Y <= boundingBox.Item2.Y)
                    {
                        yield return point2;
                    }
                }
            }
        }
    }
}
