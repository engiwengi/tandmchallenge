﻿using System;
using System.Collections.Generic;
using System.Linq;

// Assumptions
// Input is a roughly rectangular room, coordinates of 4 corners are given, translation and angle of the room is arbitrary
// Ceiling height is always constant
// Sprinklers should be at most 2500mm from each other and the wall, for a wall this is a direct shortest distance from the sprinkler, not from the wall (e.g not the corner of the room)
// Connection of the sprinkler to water pipe should be a direct shortest distance
// Provided coordinates are in mm

// Implementation
// The room coordinates are translated and rotated to acquire a bounding box that should roughly be equal to the rectangular room
// The room is equally divided into columns and rows 2500mm apart (2501 used to avoid FP precision errors)
// Sprinklers are placed at the vertices of this grid within the room
// The closest water pipe is found for each sprinkler by simply finding the closest point on any water pipe for each sprinkler


// While the solution is perfectly fine for the challenge given, I felt it was too naive and only solved a very specific scenario

// To generalise the challenge I assume an arbitrary polygon is given, again with the ceiling fixed, same rules apply in regards to spinkler clearance
// A hexagon grid with side length 2500mm is overlaid on the ceiling polygon
// Any hexagon verticies contained within the polygon are therefore 2500mm from eachother and no more than 2500mm from a wall
// the grid is brute force translated one hexagon unit / rotated 60 degrees to find the layout with the least number of sprinklers 
// within the polygon and layouts where sprinklers are further from the walls are preferred

// A hexagon grid was chosen for the sprinkler layout as it has the lowest density packing ratio to completely cover an area

// Unfortunately the rules set out allow the hexagon grid to leave gaps in the corners, as technically no sprinkler is more than 2500mm from the wall, the wall itself is further than 2500mm from the sprinkler
// in some cases (same is true in the naive solution). I did not progress with the hexagon grid as I felt instead of a distance between wall and sprinkler, a radius of water protection would be 
// a better fit for this solution then the polygon can be completely covered by circles through the hexagon grid

// I considered algorithms for completely covering a polygon with circles/disks of fixed radius however
// feel it is unnecessary as it would likely make installation of the sprinklers much more difficult for marginal gains (perhaps a few less sprinklers in a large complex polygon)
// a hexagon grid is straightforward to install as it consists of straight lines

// Further implementation to consider would be breaking a complex polygon into individual parts to separately rotate / translate the hexagon grid
// in addition, upon finding a good solution, the edge sprinkler positions could probably be jittered to remove 1 or 2 sprinklers in some cases

// I am also curious if implementing a simulation with 'gravity' to push sprinklers away from each other and the wall and towards pipes to minimize pipe distance would be a good / interesting solution

namespace TandmChallenge
{
    class Program
    {
        public const double CeilingHeight = 2500.0;
        static Polygon2d Ceiling = new Polygon2d
            (
                new List<Point2d>() {
                new Point2d(97500.00, 34000.00),
                new Point2d(85647.67, 43193.61),
                new Point2d(91776.75, 51095.16),
                new Point2d(103629.07, 41901.55),
                }
            );
        static List<LineSegment3d> WaterPipes = new List<LineSegment3d>() {
                new LineSegment3d { start = new Point3d(98242.11, 36588.29, 3000.00), end = new Point3d ( 87970.10,  44556.09, 3500.00)},
                new LineSegment3d { start = new Point3d(99774.38, 38563.68, 3500.00), end = new Point3d (89502.37,  46531.47, 3000.00 )},
                new LineSegment3d { start = new Point3d( 101306.65, 40539.07,  3000.00), end = new Point3d ( 91034.63,  48506.86, 3000.00 )},
            };

        static void Main(string[] args)
        {
            var room = new RectangleRoom(Ceiling.vertices);
            var sprinklers = room.LocateSprinklers();

            // Below code can be used to view hex grid implementation
            // var sprinklers = new List<Point2d>();
            // var hexGrid = new HexGrid() { angle = 0.0, translation = new Point2d(0, 0) };
            // hexGrid.OptimizeForLeastVerticesInscribedByPolygon(Ceiling);
            // sprinklers.AddRange(hexGrid.VerticesInBoundingBox(Ceiling.boundingBox).Where(sprinkler => Ceiling.ContainsPoint(sprinkler)));

            foreach (var sprinkler in sprinklers)
            {
                Console.WriteLine($"Sprinkler:   ({sprinkler.X.ToString("0.00")}, {sprinkler.Y.ToString("0.00")}, {CeilingHeight.ToString("0.00")})");
                var sprinkler3d = sprinkler.To3d(CeilingHeight);
                var closestDistance = double.MaxValue;
                var attachment = new Point3d(0, 0, 0);
                foreach (var waterPipe in WaterPipes)
                {
                    var potentialAttachment = waterPipe.ClosestPointOnLineToPoint(sprinkler3d);
                    double attachmentDistance = (potentialAttachment - sprinkler3d).SquaredMagnitude();
                    if (closestDistance > attachmentDistance)
                    {
                        closestDistance = attachmentDistance;
                        attachment = potentialAttachment;
                    }
                }
                Console.WriteLine($"Attached at: ({attachment.X.ToString("0.00")}, {attachment.Y.ToString("0.00")}, {attachment.Z.ToString("0.00")})");
            }

            Console.WriteLine($"Total sprinklers: {sprinklers.Count}");
        }
    }
}
