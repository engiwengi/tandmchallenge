using System;
using System.Collections.Generic;
using System.Linq;
namespace TandmChallenge
{
    public struct RectangleRoom
    {
        // Slightly increase clearance to avoid FP precision errors - mostly negligible for any reasonable sized room
        public const double SprinklerClearance = 2501.0;
        public double width;
        public double length;

        public double angle;

        public Point2d translation;


        // Expects 4 points that are roughly in a rectangle
        public RectangleRoom(List<Point2d> corners)
        {
            if (corners.Count != 4)
            {
                throw new ArgumentException("Rectangle room must have exactly 4 corners");
            }
            this.translation = new Point2d(double.MaxValue, double.MaxValue);
            this.angle = 0;

            var prevVertex = corners[corners.Count - 1];

            foreach (var nextVertex in corners)
            {
                // The intention here is to choose a corner to be (0,0), it is arbitrary but I have chosen the point with the smallest Y coordinate
                if (nextVertex.Y < this.translation.Y)
                {
                    this.translation = nextVertex;
                    this.angle = Math.Atan2(nextVertex.Y - prevVertex.Y, nextVertex.X - prevVertex.X);
                }
                prevVertex = nextVertex;
            }

            double cosAngle = Math.Cos(Math.PI - this.angle);
            double sinAngle = Math.Sin(Math.PI - this.angle);

            var boundingBoxMin = new Point2d(double.MaxValue, double.MaxValue);
            var boundingBoxMax = new Point2d(double.MinValue, double.MinValue);

            foreach (var vertex in corners)
            {
                var rotated = (vertex - this.translation).RotatePrecalculated(sinAngle, cosAngle);
                boundingBoxMin.X = Math.Min(boundingBoxMin.X, rotated.X);
                boundingBoxMin.Y = Math.Min(boundingBoxMin.Y, rotated.Y);
                boundingBoxMax.X = Math.Max(boundingBoxMax.X, rotated.X);
                boundingBoxMax.Y = Math.Max(boundingBoxMax.Y, rotated.Y);
            }

            this.width = boundingBoxMax.X - boundingBoxMin.X;
            this.length = boundingBoxMax.Y - boundingBoxMin.Y;
        }

        public List<Point2d> LocateSprinklers()
        {
            int numColumns = (int)Math.Ceiling(this.width / SprinklerClearance);
            int numRows = (int)Math.Ceiling(this.length / SprinklerClearance);
            double cosAngle = -Math.Cos(this.angle);
            double sinAngle = -Math.Sin(this.angle);

            var sprinklers = new List<Point2d>();

            for (int column = 1; column < numColumns; column++)
            {
                for (int row = 1; row < numRows; row++)
                {
                    var sprinklerCoord = new Point2d(this.width / numColumns * column, this.length / numRows * row).RotatePrecalculated(sinAngle, cosAngle) + this.translation;
                    sprinklers.Add(sprinklerCoord);
                }
            }

            return sprinklers;
        }
    }
}
