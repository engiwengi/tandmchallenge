using System;

namespace TandmChallenge
{
    public class LineSegment2d
    {
        public Point2d start;
        public Point2d end;

        public bool Intersects(LineSegment2d other)
        {

            bool IsCounterClockwise(Point2d point1, Point2d point2, Point2d point3)
            {
                return (point3.Y - point1.Y) * (point2.X - point1.X) > (point2.Y - point1.Y) * (point3.X - point1.X);
            }

            return IsCounterClockwise(this.start, other.start, other.end) != IsCounterClockwise(this.end, other.start, other.end) &&
            IsCounterClockwise(this.start, this.end, other.start) != IsCounterClockwise(this.start, this.end, other.end);
        }


        public Point2d ClosestPointOnLineToPoint(Point2d point)
        {
            var percent = (point - this.start).Dot(this.end - this.start) / (this.end - this.start).SquaredMagnitude();
            return this.PointAtPercentage(percent);
        }

        private Point2d PointAtPercentage(double percentage)
        {
            if (percentage <= 0)
            {
                return this.start;
            }
            else if (percentage >= 1)
            {
                return this.end;
            }
            else
            {
                return new Point2d(
                    this.start.X + (this.end.X - this.start.X) * percentage,
                   this.start.Y + (this.end.Y - this.start.Y) * percentage);
            }
        }
    }
}
