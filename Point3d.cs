using System;

namespace TandmChallenge
{
    public class Point3d
    {
        public double X { get; set; }
        public double Y { get; set; }
        public double Z { get; set; }


        public Point3d(double x, double y, double z)
        {
            this.X = x;
            this.Y = y;
            this.Z = z;
        }


        public static Point3d operator -(Point3d a, Point3d b)
        {
            return new Point3d(a.X - b.X, a.Y - b.Y, a.Z - b.Z);
        }
        public static Point3d operator +(Point3d a, Point3d b)
        {
            return new Point3d(a.X + b.X, a.Y + b.Y, a.Z + b.Z);
        }
        public static Point3d operator *(Point3d a, double b)
        {
            return new Point3d(a.X * b, a.Y * b, a.Z * b);
        }



        public double Dot(Point3d other)
        {
            return this.X * other.X + this.Y * other.Y + this.Z * other.Z;
        }

        public double SquaredMagnitude()
        {
            return this.Dot(this);
        }

        public double SquaredDistanceBetweenPoints(Point3d other)
        {
            return (this - other).SquaredMagnitude();
        }
    }
}
