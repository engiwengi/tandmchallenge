using System;
using System.Diagnostics.CodeAnalysis;

namespace TandmChallenge
{
    public class Point2d
    {
        public double X { get; set; }
        public double Y { get; set; }

        public static Point2d operator -(Point2d a, Point2d b)
        {
            return new Point2d(a.X - b.X, a.Y - b.Y);
        }

        public static Point2d operator *(Point2d a, double b)
        {
            return new Point2d(a.X * b, a.Y * b);
        }
        public static Point2d operator /(Point2d a, double b)
        {
            return new Point2d(a.X / b, a.Y / b);
        }
        public static Point2d operator +(Point2d a, Point2d b)
        {
            return new Point2d(a.X + b.X, a.Y + b.Y);
        }

        public Point2d(double x, double y)
        {
            this.X = x;
            this.Y = y;
        }

        public double Dot(Point2d other)
        {
            return this.X * other.X + this.Y * other.Y;
        }

        public double SquaredMagnitude()
        {
            return this.Dot(this);
        }


        public Point2d Clone()
        {
            return new Point2d(this.X, this.Y);
        }

        public Point2d RotatePrecalculated(double sinAngle, double cosAngle)
        {
            return new Point2d(this.X * cosAngle - this.Y * sinAngle, this.X * sinAngle + this.Y * cosAngle);
        }

        public Point3d To3d(double z)
        {
            return new Point3d(this.X, this.Y, z);
        }
    }
}
